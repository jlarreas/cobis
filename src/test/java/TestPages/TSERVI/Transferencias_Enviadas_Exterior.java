package TestPages.TSERVI;

import Globales.Util;
import org.openqa.selenium.WebElement;

public class Transferencias_Enviadas_Exterior {


    void clic_menu_S_varios() throws InterruptedException {
        WebElement menu_s_varios= Util.driver.findElementByName("S. Varios");
        String actual=menu_s_varios.getText();
        Util.assert_contiene("Pago de Transferencia enviada al exterior", "Da click en el men� S. Varios", actual, "S. Varios", true, "C");
        menu_s_varios.click();
        Thread.sleep(1000);
    }

    void clic_submenu_Transferencias() throws InterruptedException {
        WebElement menu_transferencias= Util.driver.findElementByName("Transferencias");
        String actual=menu_transferencias.getText();
        Util.assert_contiene("Pago de Transferencia enviada al exterior", "Da click en el men� Transferencias", actual, "Transferencias", true, "C");
        menu_transferencias.click();
        Thread.sleep(1000);
    }
    void clic_submenu_() throws InterruptedException {
        WebElement menu_transferencias= Util.driver.findElementByName("Transferencias");
        String actual=menu_transferencias.getText();
        Util.assert_contiene("Pago de Transferencia enviada al exterior", "Da click en el men� Transferencias", actual, "Transferencias", true, "C");
        menu_transferencias.click();
        Thread.sleep(1000);
    }

    public void generar() throws InterruptedException {
        this.clic_menu_S_varios();
        this.clic_submenu_Transferencias();
    }
}
