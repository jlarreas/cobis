package TestPages.TSERVI;

import Globales.Reporte;
import Globales.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class Login {
    String[] datos = null;

    void cargaDatosLogin(String seclogin) {
        if (seclogin.equals("0"))
            seclogin = Util.prop.getProperty("sec_login");

        datos = Util.getCamposDataPool(System.getProperty("user.dir") + "/archivos/dp_login.txt", seclogin);
        if (datos == null)
            System.out.println("No hay datos en datapool");
        else {
            Util.setDataCliente(datos);
            System.out.println("setUp==>Datos Leidos de datapool");
        }
    }

    void clic_btn_maximizar() {
        WebElement btn_maximizar = Util.driver.findElement(By.name("Maximizar"));
        btn_maximizar.click();

    }

    void clic_btn_conexion() {
        WebElement btn_logon = Util.driver.findElement(By.name("Conexión"));
        String actual = btn_logon.getText();
        Util.assert_contiene("LOGIN", "Dar click en Conexión", actual, "Conexión", true, "C");
        btn_logon.click();
    }


    void clic_menu_logon() {
        WebElement menu_logon = Util.driver.findElement(By.name("Log on"));
        String actual = menu_logon.getText();
        Util.assert_contiene("LOGIN", "Dar clic en Log On ", actual, "Log on", true, "C");
        menu_logon.click();
    }

    void ingreso_datos1() {
        WebElement servidor = Util.driver.findElementByAccessibilityId("5");
        servidor.sendKeys(Util.getDataCliente()[1]);
        WebElement user = Util.driver.findElementByAccessibilityId("2");
        user.sendKeys(Util.getDataCliente()[2]);
        WebElement password = Util.driver.findElementByAccessibilityId("1");
        password.sendKeys(Util.getDataCliente()[3]);
    }

    void ingreso_datos2() {
        String actual = "Servidor: " + Util.getDataCliente()[1] + "</b><br>"
                + "Usuario: " + Util.getDataCliente()[2] + "</b><br>" + "Contraseña: " + Util.getDataCliente()[3];
        Reporte.agregarPaso("LOGIN", "Ingresa datos de login", actual, "", true, "C");
    }

    void click_boton_ok() {
        WebElement boton = Util.driver.findElement(By.name("OK"));
        String actual = boton.getText();
        Util.assert_contiene("LOGIN", "Da click en botón OK", actual, "OK", true, "C");
        boton.click();
    }

    void click_boton_aceptar() {
        WebElement boton = Util.driver.findElement(By.className("Button"));
        String actual = boton.getText();
        Util.assert_contiene("LOGIN", "Da click en botón Aceptar", actual, "Aceptar", true, "C");
        boton.click();
    }

    void escogeRol() {
        int i = 0;
        while (i != Integer.parseInt(Util.getDataCliente()[5])) {
            Util.driver.findElementByName("Línea abajo").click();
            i++;
        }
        Actions action = new Actions(Util.driver);
        action.moveToElement(Util.driver.findElementByName("Datos Generales"), 0, 0).moveByOffset(40, 170).click().perform();

    }



    public void ingresar() throws InterruptedException {
        Thread.sleep(2000);
        String[] allWindowHandles = Util.driver.getWindowHandles().toArray(new String[0]);
        Util.driver.switchTo().window(allWindowHandles[0]);
        this.cargaDatosLogin("2");
        this.clic_btn_maximizar();
        this.clic_btn_conexion();
        this.clic_menu_logon();
        this.ingreso_datos1();
        this.ingreso_datos2();
        this.click_boton_ok();
        this.click_boton_aceptar();
        this.escogeRol();
        this.click_boton_ok();
        this.click_boton_aceptar();
    }

}
