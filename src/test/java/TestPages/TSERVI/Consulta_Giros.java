package TestPages.TSERVI;

import Globales.Reporte;
import Globales.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class Consulta_Giros {
    String[] datos = null;

    void cargaDatosLogin(String seclogin) {
        if (seclogin.equals("0"))
            seclogin = Util.prop.getProperty("sec_login");

        datos = Util.getCamposDataPool(System.getProperty("user.dir") + "/archivos/dp_consulta_giros.txt", seclogin);
        if (datos == null)
            System.out.println("No hay datos en datapool");
        else {
            Util.setDataCliente(datos);
            System.out.println("setUp==>Datos Leidos de datapool");
        }
    }
    void clic_menu_giros()
    {

        WebElement menu_giros=Util.driver.findElementByName("Giros");
        String actual=menu_giros.getText();
        Util.assert_contiene("Consulta Giros", "Da click en el menú Giros", actual, "Giros", true, "C");
        menu_giros.click();
    }
    void clic_submenu_consultas()
    {
        WebElement menu_consultas=Util.driver.findElementByName("Consultas");
        String actual=menu_consultas.getText();
        Util.assert_contiene("Consulta Giros", "Da click en submenú Consultas", actual, "Consultas", true, "C");
        menu_consultas.click();
    }
    void clic_submenu_ConsultaGiros() throws InterruptedException {
        WebElement submenu_consultagiros=Util.driver.findElementByName("Consultas de Giros");
        String actual=submenu_consultagiros.getText();
        Util.assert_contiene("Consultas Giros", "Da click en submenú Consultas de Giros", actual, "Consultas de Giros", true, "C");
        submenu_consultagiros.click();
        Thread.sleep(2000);
    }

    void clic_estado_giros()
    {
        WebElement estado_giro=Util.driver.findElementByAccessibilityId("11");
        estado_giro.sendKeys(Util.getDataCliente()[1]);
        Reporte.agregarPaso("Consultar giros", "Ingresa estado del giro", "", "", true, "N");

    }
    void clic_agente_pagador()
    {
        WebElement agente_pagador=Util.driver.findElementByAccessibilityId("3");
        agente_pagador.sendKeys(Util.getDataCliente()[2]);
       // Reporte.agregarPaso("Consultar giros", "Ingresa agente Pagador", "", "mostrar giros con agente pagador: "+agente_pagador, true, "N");

    }
    void clic_fechainicio(){
        List<WebElement> fechainicio=Util.driver.findElements(By.className("Edit"));
        fechainicio.get(2).sendKeys(Util.getDataCliente()[3]);
        Reporte.agregarPaso("Consultar giros", "Ingreso de  fecha de inicio", "", "mostrar giros con fecha inicio: "+fechainicio, true, "N");


    }
    void clic_fechafin(){
        List<WebElement> fechafin=Util.driver.findElements(By.className("Edit"));
        fechafin.get(1).sendKeys(Util.getDataCliente()[4]);
        Reporte.agregarPaso("Consultar giros", "Ingreso de  fecha de fin", "", "mostrar giros con fecha fin: " +fechafin, true, "N");


    }
    void clic_corresponsal()
    {
        WebElement corresponsal=Util.driver.findElementByAccessibilityId("8");
        corresponsal.sendKeys(Util.getDataCliente()[5]);
        Reporte.agregarPaso("Consultar giros", "Ingreso de corresponsal", "", "mostrar giros del corresponsal: "+corresponsal, true, "N");

    }
    void clic_remitente()
    {
        WebElement remitente=Util.driver.findElementByAccessibilityId("4");
        remitente.sendKeys(Util.getDataCliente()[6]);
      //  Reporte.agregarPaso("Consultar giros", "Ingreso de remitente", "", "mostrar giros del remitente: "+remitente, true, "N");

    }
    void clic_beneficiario()
    {
        WebElement beneficiario=Util.driver.findElementByAccessibilityId("7");
        beneficiario.sendKeys(Util.getDataCliente()[7]);
        //  Reporte.agregarPaso("Consultar giros", "Ingreso de benficiario", "", "mostrar giros del beneficiario: "+beneficiario, true, "N");

    }
    void clic_codigo_giro()
    {
        WebElement codigo_giro=Util.driver.findElementByAccessibilityId("5");
        codigo_giro.sendKeys(Util.getDataCliente()[8]);
        Reporte.agregarPaso("Consultar giros", "Ingreso de código de giro", "", "mostrar giros con códgio: "+codigo_giro, true, "N");

    }
    void clic_n_transferencia()
    {
        WebElement transferencia=Util.driver.findElementByAccessibilityId("6");
        transferencia.sendKeys(Util.getDataCliente()[9]);
       // Reporte.agregarPaso("Consultar giros", "Ingreso de número de transfererencia de un giro", "", "mostrar giro con número de transferencia: "+transferencia, true, "N");

    }
    void clic_btn_buscar() throws InterruptedException {
        WebElement button_buscar=Util.driver.findElement(By.name("Buscar"));
        button_buscar.click();
        Thread.sleep(2000);
        Reporte.agregarPaso("Consultar giros", "Ingreso de número de transfererencia de un giro", "", "mostrar giro con número de transferencia: ", true, "N");

    }


    public void consulta() throws InterruptedException {
        Thread.sleep(2000);
        String[] allWindowHandles = Util.driver.getWindowHandles().toArray(new String[0]);
        Util.driver.switchTo().window(allWindowHandles[0]);

        this.cargaDatosLogin("2");
        this.clic_menu_giros();
        this.clic_submenu_consultas();
        this.clic_submenu_ConsultaGiros();
        this.clic_estado_giros();
        this.clic_agente_pagador();
        this.clic_fechainicio();
        this.clic_fechafin();
        this.clic_corresponsal();
        this.clic_remitente();
        this.clic_beneficiario();
        this.clic_codigo_giro();
        this.clic_n_transferencia();
        this.clic_btn_buscar();
}
}
