package TestPages.TSERVI;

import Globales.Reporte;
import Globales.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class ConsultaTransferenciaExterior {

    String[] datos = null;

    void cargaDatosLogin(String seclogin) {
        if (seclogin.equals("0"))
            seclogin = Util.prop.getProperty("sec_login");

        datos = Util.getCamposDataPool(System.getProperty("user.dir") + "/archivos/dp_consulta_transferencias.txt", seclogin);
        if (datos == null)
            System.out.println("No hay datos en datapool");
        else {
            Util.setDataCliente(datos);
            System.out.println("setUp==>Datos Leidos de datapool");
        }
    }


    void clic_menu_S_varios() throws InterruptedException {
        WebElement menu_s_varios= Util.driver.findElementByName("S. Varios");
        String actual=menu_s_varios.getText();
        Util.assert_contiene("Consultas Transferencias Exterior", "Da click en el menú S. Varios", actual, "S. Varios", true, "C");
        menu_s_varios.click();
        Thread.sleep(1000);
    }

    void clic_submenu_Transferencias() throws InterruptedException {
        WebElement menu_transferencias= Util.driver.findElementByName("Transferencias");
        String actual=menu_transferencias.getText();
        Util.assert_contiene("Consultas Transferencias Exterior", "Da click en el menú Transferencias", actual, "Transferencias", true, "C");
        menu_transferencias.click();
        Thread.sleep(1000);
    }

    void clic_submenu_Consulta_Historica_Enviadas() throws InterruptedException {
        WebElement menu_transferencias= Util.driver.findElementByName("Consulta Historica Enviadas");
        String actual=menu_transferencias.getText();
        Util.assert_contiene("Consultas Transferencias Exterior", "Da click en el menú Consulta Historica Enviadas", actual, "Consulta Historica Enviadas", true, "C");
        menu_transferencias.click();
        Thread.sleep(1000);
    }

    void clic_Estado_Transferencia()
    {
        WebElement input_Estado=Util.driver.findElementByAccessibilityId("10");
        input_Estado.sendKeys(Util.getDataCliente()[1]);
        Reporte.agregarPaso("Consultas Transferencias Exterior", "Ingresar Estado del Giro", "", "", true, "N");

    }

    void clic_fechainicio()
    {
        List<WebElement> date_fechainicio=Util.driver.findElements(By.className("DTPicker20WndClass"));
        date_fechainicio.get(1).sendKeys(Util.getDataCliente()[2]);
        Reporte.agregarPaso("Consultas Transferencias Exterior", "Ingresar fecha de inicio", "", "", true, "N");

    }
    void clic_fechafin()
    {
        List<WebElement> date_fechafin=Util.driver.findElements(By.className("DTPicker20WndClass"));
        date_fechafin.get(0).sendKeys(Util.getDataCliente()[3]);
        Reporte.agregarPaso("Consultas Transferencias Exterior", "Ingresar fecha de fin", "", "", true, "N");

    }

    void clic_button_buscar() throws InterruptedException {
        WebElement btn_buscar= Util.driver.findElement(By.name("Buscar"));
        String actual=btn_buscar.getText();
        Util.assert_contiene("Consultas Transferencias Exterior", "Da click en el botón Buscar", actual, "Buscar", true, "C");
        btn_buscar.click();
        Thread.sleep(3000);
    }


    public void consultar_transf_exterior() throws InterruptedException {
        this.cargaDatosLogin("1");
        this.clic_menu_S_varios();
        this.clic_submenu_Transferencias();
        this.clic_submenu_Consulta_Historica_Enviadas();
        this.clic_Estado_Transferencia();
        this.clic_fechainicio();
        this.clic_fechafin();
        this.clic_button_buscar();
    }
}
