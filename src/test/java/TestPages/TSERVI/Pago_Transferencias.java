package TestPages.TSERVI;

import Globales.Reporte;
import Globales.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Pago_Transferencias {

    void clic_menu_S_varios() throws InterruptedException {
        WebElement menu_s_varios= Util.driver.findElementByName("S. Varios");
        String actual=menu_s_varios.getText();
        Util.assert_contiene("Pago de Transferencia enviada al exterior", "Da click en el menú S. Varios", actual, "S. Varios", true, "C");
        menu_s_varios.click();
        Thread.sleep(1000);
    }

    void clic_submenu_Transferencias() throws InterruptedException {
        WebElement menu_transferencias= Util.driver.findElementByName("Transferencias");
        String actual=menu_transferencias.getText();
        Util.assert_contiene("Pago de Transferencia enviada al exterior", "Da click en el menú Transferencias", actual, "Transferencias", true, "C");
        menu_transferencias.click();
        Thread.sleep(1000);
    }

    void clic_submenu_aprobacion() throws InterruptedException
    {
        WebElement menu_aprobacion_transferencias= Util.driver.findElement(By.name("Aprobación Solicitud"));
        String actual=menu_aprobacion_transferencias.getText();
        Util.assert_contiene("Pago de Transferencia enviada al exterior", "Da click en el menú Aprobacion de Solicitud", actual, "Aprobación Solicitud", true, "C");
        menu_aprobacion_transferencias.click();
        Thread.sleep(1000);
    }
    void estado_transferencia()
    {
        WebElement estado_transferencia=Util.driver.findElementByAccessibilityId("3");
        estado_transferencia.sendKeys("V");
        Reporte.agregarPaso("Pago de Transferencia enviada al exterior", "Ingresar estado de transferencia", "", "", true, "N");

    }
    void clic_button_buscar() throws InterruptedException {
        WebElement btn_buscar=Util.driver.findElementByName("Buscar");
        String actual=btn_buscar.getText();
        Util.assert_contiene("Pago de Transferencia enviada al exterior", "Da click en el boton Buscar", actual, "Buscar", true, "C");
        btn_buscar.click();
        Thread.sleep(2000);
    }

  /*
              Util.wdriver.findElementByName("S. Varios").click();
    // Reporte.agregarPaso("Anulacion de Transferencias", "Buscar la transferencia antes de la anulación", "", "", true, "N");
           Util.wdriver.findElementByName("Transferencias").click();
    // Reporte.agregarPaso("Anulacion de Transferencias", "Buscar la transferencia antes de la anulación", "", "", true, "N");
           Util.wdriver.findElementByAccessibilityId("167").click();
           Thread.sleep(2000);

           Util.wdriver.findElementByAccessibilityId("3").click();
           Util.wdriver.findElementByAccessibilityId("3").sendKeys("P");
           Util.wdriver.findElementByAccessibilityId("3").click();
    Actions actions=new Actions(Util.wdriver);
          for(int i=0;i <2;i++){//permite moverse a la fecha de fin
        actions.sendKeys(Keys.TAB).perform();
    }
          actions.sendKeys("06/30/2021").perform();
          actions.sendKeys(Keys.TAB).perform();//TAB Fecha de inicio
          actions.sendKeys("06/01/2021").perform();


   */

    public void pago() throws InterruptedException {
        this.clic_menu_S_varios();
        this.clic_submenu_Transferencias();
        this.clic_submenu_aprobacion();
        this.estado_transferencia();
        this.clic_button_buscar();
    }
}
