package TestPages.TSERVI;

import Globales.Reporte;
import Globales.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

public class Solicitud_Certificado {
    void clic_menu_giros()
    {
        WebElement menu_giros=Util.driver.findElementByName("Giros");
        String actual=menu_giros.getText();
        Util.assert_contiene("Consulta Giros", "Da click en el menú Giros", actual, "Giros", true, "C");
        menu_giros.click();
    }
    void clic_menu_solicitud_certificado()
    {

        WebElement menu_solicitud_certificado=Util.driver.findElementByName("Solicitud de Certificado");
        String actual=menu_solicitud_certificado.getText();
        Util.assert_contiene("Solicitud  Giros", "Da click en el menú Solicitud de Certificado", actual, "Solicitud de Certificado", true, "C");
        menu_solicitud_certificado.click();
    }
    void clic_F5() throws InterruptedException
    {

        WebElement clic_f5=Util.driver.findElementByAccessibilityId("4");
        clic_f5.sendKeys(Keys.F5);
        Reporte.agregarPaso("Solicitud de Certificado Giros", "Dar clic botón F5", "", "", true, "N");
        Thread.sleep(2000);

    }

    void clic_radioButton_mis()
    {
        String[] allWindowHandles = Util.driver.getWindowHandles().toArray(new String[0]);//permite ir a la sgte ventana
        Util.driver.switchTo().window(allWindowHandles[0]);
        List<WebElement> lista2 = Util.driver.findElementsByAccessibilityId("2");
        lista2.get(0).click();
    }
    void clic_valor_busqueda(){
        WebElement ventana=Util.driver.findElement(By.className("ThunderRT6FormDC"));
        ventana.click();
        Actions moverse=new Actions(Util.driver);
        moverse.moveToElement(Util.driver.findElement(By.className("ThunderRT6FormDC")),0,0).moveByOffset(160,30).click().perform();
        moverse.sendKeys(Keys.ARROW_LEFT).perform();
        Reporte.agregarPaso("Solicitud de Certificado Giros", "Ingresar número Cédula", "", "", true, "N");
        moverse.sendKeys("0904894243").perform();
    }
    void clic_button_buscar()
    {
        WebElement ventana=Util.driver.findElement(By.className("ThunderRT6FormDC"));
        ventana.click();
        Actions clic_btn_buscar=new Actions(Util.driver);
        Reporte.agregarPaso("Solicitud de Certificado Giros", "Dar clic botón Buscar", "", "", true, "N");
        clic_btn_buscar.moveToElement(Util.driver.findElement(By.className("ThunderRT6FormDC")),0,0).moveByOffset(600,3).click().perform();
    }
    void seleccionar_grid_cliente() throws InterruptedException {
        Thread.sleep(2000);
        Actions moverse=new Actions(Util.driver);
        moverse.moveToElement(Util.driver.findElement(By.className("ThunderRT6FormDC")),0,0).moveByOffset(50,100).click().perform();
    }

    void clic_button_escoger() throws InterruptedException
    {
        WebElement ventana=Util.driver.findElement(By.className("ThunderRT6FormDC"));
        ventana.click();
        Actions clic_btn_escoger=new Actions(Util.driver);
            Reporte.agregarPaso("Solicitud de Certificado Giros", "Dar clic botón Escoger", "", "", true, "N");
        clic_btn_escoger.moveToElement(Util.driver.findElement(By.className("ThunderRT6FormDC")),0,0).moveByOffset(600,90).click().perform();

    }
    void clic_button_buscar_Solicitud() throws InterruptedException {
        WebElement button_buscar=Util.driver.findElement(By.name("Buscar"));
        Reporte.agregarPaso("Solicitud de Certificado Giros", "Dar clic botón Buscar", "", "", true, "N");
        button_buscar.click();
        Thread.sleep(2000);
    }
    void clic_button_siguiente()
    {
        for(int i=0;i <15;i++){//clic boton siguiente
            WebElement button_siguiente=Util.driver.findElement(By.name("Siguiente"));
            button_siguiente.click();
        }
    }

    void clic_button_transmitir() {
        WebElement button_transmitir=Util.driver.findElement(By.name("Transmitir"));
        Reporte.agregarPaso("Solicitud de Certificado Giros", "Dar clic botón Transmitir", "", "", true, "N");
        button_transmitir.click();
    }
    void clic_mensaje_Si()
    {
        WebElement pantalla_Atencion=Util.driver.findElement(By.className("#32770"));
        pantalla_Atencion.click();
        WebElement button_si=Util.driver.findElement(By.name("Sí"));
        Reporte.agregarPaso("Solicitud de Certificado Giros", "Dar clic botón Sí", "", "", true, "N");
        button_si.click();
    }
    void clic_button_aceptar()
    {
        WebElement pantalla_Atencion=Util.driver.findElement(By.className("#32770"));
        pantalla_Atencion.click();
        WebElement button_aceptar=Util.driver.findElement(By.name("Aceptar"));
        Reporte.agregarPaso("Solicitud de Certificado Giros", "Dar clic botón Aceptar", "", "", true, "N");
        button_aceptar.click();
    }

    void cambiar_pantalla_certificado() throws InterruptedException
    {
        String[] allWindowHandles2 = Util.driver.getWindowHandles().toArray(new String[0]);//permite ir a la sgte ventana
        Util.driver.switchTo().window(allWindowHandles2[1]);
        Thread.sleep(6000);

    }

    void clic_buttonn_maximizar()
    {
        WebElement button_maximizar = Util.driver.findElement(By.name("Maximizar"));
        button_maximizar.click();
    }
    void clic_button_exportar()
    {
        Actions moverse= new Actions(Util.driver);
        moverse.moveToElement(Util.driver.findElementByAccessibilityId("59392"),0,0).moveByOffset(250,5).perform();
        Reporte.agregarPaso("Solicitud de Certificado Giros", "Dar clic botón Exportar", "", "", true, "N");
        moverse.click().perform();
    }
    void clic_button_abrir()
    {
        Util.driver.findElementByName("Abrir").click();
      //  Reporte.agregarPaso("Solicitud de Certificado Giros", "Ingresar el tipo de archivo a exportar", "", "", true, "N");
        Actions elegir_formato= new Actions(Util.driver);
        elegir_formato.sendKeys("RICH").perform();//elegir el formarto
    }
    void clic_button_ok()
    {
        WebElement button_ok=Util.driver.findElementByName("OK");
        Reporte.agregarPaso("Solicitud de Certificado Giros", "Dar clic botón ok", "", "", true, "N");
        button_ok.click();
    }
    void pantalla_ChooseExportFile()
    {
        WebElement pantalla_exportar=Util.driver.findElement(By.name("Choose Export File"));
        Reporte.agregarPaso("Solicitud de Certificado Giros", "Guardar el archivo", "", "", true, "N");
        pantalla_exportar.click();
    }
    void clic_button_Guardar() throws InterruptedException {
        WebElement button_guardar=Util.driver.findElement(By.name("Guardar"));
        Reporte.agregarPaso("Solicitud de Certificado Giros", "Dar clic botón Guardar", "", "", true, "N");
        button_guardar.click();
        Thread.sleep(2000);
        Actions elegir_formato= new Actions(Util.driver);
        elegir_formato.sendKeys(Keys.ENTER).perform();
    }
    public void generar() throws InterruptedException {
        this.clic_menu_giros();
        this.clic_menu_solicitud_certificado();
        this.clic_F5();
      //this.clic_radioButton_mis();
        this.clic_valor_busqueda();
        this.clic_button_buscar();
        this.seleccionar_grid_cliente();
        this.clic_button_escoger();
        this.clic_button_buscar_Solicitud();
        this.clic_button_siguiente();
        this.clic_button_transmitir();
        this.clic_mensaje_Si();
        this.clic_button_aceptar();
        this.cambiar_pantalla_certificado();
        this.clic_buttonn_maximizar();
        this.clic_button_exportar();
        this.clic_button_abrir();
        this.clic_button_ok();
        this.pantalla_ChooseExportFile();
        this.clic_button_Guardar();
    }
}
