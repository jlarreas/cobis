package TestPages.TSERVI;

import Globales.Reporte;
import Globales.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Anulacion_Giros {

    String[] datos = null;

    void cargaDatosLogin(String seclogin) {
        if (seclogin.equals("0"))
            seclogin = Util.prop.getProperty("sec_login");

        datos = Util.getCamposDataPool(System.getProperty("user.dir") + "/archivos/dp_anulacion_giros.txt", seclogin);
        if (datos == null)
            System.out.println("No hay datos en datapool");
        else {
            Util.setDataCliente(datos);
            System.out.println("setUp==>Datos Leidos de datapool");
        }
    }

    void clic_menu_giros()
    {
        WebElement menu_giros=Util.driver.findElementByName("Giros");
        String actual=menu_giros.getText();
        Util.assert_contiene("Anulación Giros", "Da click en el menú Giros", actual, "Giros", true, "C");
        menu_giros.click();
    }
    void clic_submenu_anulaciongiros() throws InterruptedException
    {
        WebElement menu_anulacion=Util.driver.findElementByName("Anulacion de Giros");
        String actual=menu_anulacion.getText();
        Util.assert_contiene("Anulación Giros", "Da click en el menú Anulacion de Giros", actual, "Anulacion de Giros", true, "C");
        menu_anulacion.click();
        Thread.sleep(3000);
    }
        void clic_codigo_giro() throws InterruptedException {
        String[] allWindowHandles = Util.driver.getWindowHandles().toArray(new String[0]);//permite ir a la sgte ventana
        Util.driver.switchTo().window(allWindowHandles[0]);
        WebElement codigo_giro=Util.driver.findElementByAccessibilityId("6");
        codigo_giro.sendKeys(Util.getDataCliente()[1]);
        Reporte.agregarPaso("Anulación giros", "Ingreso del codigo de un giro", "", "mostrar giro con número de código: "+codigo_giro, true, "N");

    }
    void clic_n_transferencia() throws InterruptedException {
        WebElement transferencia=Util.driver.findElementByAccessibilityId("11");
        transferencia.sendKeys(Util.getDataCliente()[2]);
        Reporte.agregarPaso("Anulación giros", "Ingreso del transferencia de un giro", "", "mostrar giro con número de transferencia: "+transferencia, true, "N");
    }
    void clic_btn_buscar() throws InterruptedException {
        WebElement button_buscar=Util.driver.findElement(By.name("Buscar"));
        String actual=button_buscar.getText();
        Thread.sleep(2000);
        Reporte.agregarPaso("Consultar giros", "Dar clic en botto buscar", actual, "Buscar", true, "N");
        button_buscar.click();
    }

    void validacion_giroNoExiste()
    {
        try {
            Util.waitForElementToBeClickable(By.name("Error, Giro No Existe"));
            WebElement texto=Util.driver.findElementByName("Error, Giro No Existe");
            String actual = texto.getText();
            Util.assert_contiene("Anulación giros", "Verificación que el giro existe", actual, "Error, Giro No Existe", true, "N");
            this.clic_btn_aceptar();
        }
        catch(Exception e)
        {
            System.out.println("Giro existe, continuamos");
        }

    }


    void clic_mensaje() throws InterruptedException {
        WebElement mensaje= Util.driver.findElementByAccessibilityId("7");
        mensaje.sendKeys(Util.getDataCliente()[3]);
        Reporte.agregarPaso("Anulación giros", "Ingreso del mensaje de anulación", "", "", true, "N");
    }
    void clic_motivo() throws InterruptedException {
        WebElement motivo= Util.driver.findElementByAccessibilityId("5");
        motivo.sendKeys(Util.getDataCliente()[4]);
        Reporte.agregarPaso("Anulación giros", "Ingreso del motivo de anulación", "", "", true, "N");
    }

    void clic_observacion() throws InterruptedException {
        WebElement observacion= Util.driver.findElementByAccessibilityId("4");
        observacion.sendKeys(Util.getDataCliente()[5]);
        Reporte.agregarPaso("Anulación giros", "Ingreso de observación de la anulación", "", "", true, "N");
    }
    void clic_btn_anular() throws InterruptedException {
        WebElement btn_anular= Util.driver.findElementByName("Anular");
        String actual=btn_anular.getText();
        Reporte.agregarPaso("Anulación giros", "Dar clic en botton anular", actual, "Anular", true, "N");
        btn_anular.click();
    }
    void clicl_btn_si()
    {
        WebElement btn_si= Util.driver.findElementByAccessibilityId("6");
        String actual=btn_si.getText();
        Reporte.agregarPaso("Anulación giros", "Dar clic botón Sí", actual, "Sí", true, "N");
        btn_si.click();
    }
        void clic_btn_aceptar()
    {
        WebElement btn_aceptar=Util.driver.findElementByName("Aceptar");
        String actual=btn_aceptar.getText();
        Reporte.agregarPaso("Anulación giros", "Dar clic botón Aceptar", actual, "Aceptar", true, "N");
        btn_aceptar.click();
    }
    void clic_btn_salir()
    {
        WebElement btn_salir=Util.driver.findElementByName("Salir");
        String actual=btn_salir.getText();
        Reporte.agregarPaso("Anulación giros", "Dar clic botón Salir", actual, "Salir", true, "N");
        btn_salir.click();
    }

    public void anulacion() throws InterruptedException {
        this.cargaDatosLogin("1");
        this.clic_menu_giros();
        this.clic_submenu_anulaciongiros();
        this.clic_codigo_giro();
        //this.clic_n_transferencia();
        this.clic_btn_buscar();
        this.validacion_giroNoExiste();
        this.clic_mensaje();
        this.clic_motivo();
        this.clic_observacion();
        this.clic_btn_anular();
        this.clicl_btn_si();
        this.clic_btn_aceptar();
        this.clic_btn_salir();

    }
}
