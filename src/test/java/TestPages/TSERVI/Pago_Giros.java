package TestPages.TSERVI;

import Globales.Reporte;
import Globales.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

public class Pago_Giros {
    public int x=50;
    public int y=185;
    void clic_menu_giros()
    {
        WebElement menu_giros= Util.driver.findElementByName("Giros");
        String actual=menu_giros.getText();
        Util.assert_contiene("Pago Giros", "Da click en el menú Giros", actual, "Giros", true, "C");
        menu_giros.click();
    }


    void clic_submenu_opcioncredito() throws InterruptedException {
        WebElement menu_opcioncredito= Util.driver.findElementByName("Opcion Credito de Giros");
        String actual=menu_opcioncredito.getText();
        Util.assert_contiene("Pago Giros", "Da click en el menú Opcion Credito de Giros", actual, "Opcion Credito de Giros", true, "C");
        menu_opcioncredito.click();
        Thread.sleep(1000);
    }
    void clic_fechainicio(){
        List<WebElement> date_fechainicio=Util.driver.findElements(By.className("DTPicker20WndClass"));
        date_fechainicio.get(0).sendKeys("06/21/2021");
        Reporte.agregarPaso("Pago Giros", "Ingreso del fecha inicio", "", "", true, "N");

    }

    void clic_fechafin()
    {
        List<WebElement> date_fechainicio=Util.driver.findElements(By.className("DTPicker20WndClass"));
        date_fechainicio.get(1).sendKeys("06/23/2021");
        Reporte.agregarPaso("Pago Giros", "Ingreso del fecha fin", "", "", true, "N");

    }

    void clic_corresponsal()
    {
        WebElement input_corresponsal=Util.driver.findElementByAccessibilityId("10");
        input_corresponsal.sendKeys("123");
        Reporte.agregarPaso("Pago Giros", "Ingreso del corresponsal", "", "", true, "N");
    }

    void clic_cuenta()
    {
        WebElement input_cuenta=Util.driver.findElementByAccessibilityId("7");
        input_cuenta.sendKeys("1211063711");
        Reporte.agregarPaso("Pago Giros", "Ingreso de la cuenta para pago ", "", "", true, "N");
    }
    void clic_button_buscar() throws InterruptedException {
        WebElement btn_buscar= Util.driver.findElement(By.name("Buscar"));
        String actual=btn_buscar.getText();
        Reporte.agregarPaso("Pago Giros", "Dar clic botón buscar", actual, "Buscar", true, "C");
        btn_buscar.click();
        Thread.sleep(3000);
    }
    void grid_giros_a_pagar()
    {
        for(int i=0;i<1;i++) {//cantidad de giros que se va a pagar
            int au=15;
            y=y+au;
            Actions seleccionar=new Actions(Util.driver);
            seleccionar.moveToElement(Util.driver.findElementByAccessibilityId("32768"),0,0).moveByOffset(x,y).perform();
            seleccionar.click().perform();
        }
    }
    void clic_button_pagar() throws InterruptedException {
        WebElement btn_pagar=Util.driver.findElementByName("Pagar");
        String actual=btn_pagar.getText();
        Reporte.agregarPaso("Pago Giros", "Dar clic botón buscar", actual, "Pagar", true, "C");
        btn_pagar.click();
        Thread.sleep(6000);
    }

        public void pago_giros() throws InterruptedException {
            this.clic_menu_giros();
            this.clic_submenu_opcioncredito();
            this.clic_fechainicio();
            this.clic_fechafin();
            this.clic_corresponsal();
          //this.clic_cuenta();
            this.clic_button_buscar();
            this.grid_giros_a_pagar();
            this.clic_button_pagar();
        }
    }



