package RunScripts.TSERVI;

import Globales.Reporte;
import Globales.Util;
import TestPages.TSERVI.Anulacion_Giros;
import TestPages.TSERVI.Consulta_Giros;
import TestPages.TSERVI.Login;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.net.MalformedURLException;

public class RunAnulacionGiros {

    @Before
    public void inicioTservi() throws MalformedURLException {
        Util.Inicio("Anulacion Giros");
        Reporte.setEntorno("Ambiente: Desarrollo1" + "</b><br>" + "Modulo: Cobis-Tservi " );

    }

    @Test
    public void Anulacion_giros() throws InterruptedException {
        Reporte.setNombreReporte("Anulación Exitosa");
        Login login=new Login();
        login.ingresar();
        Anulacion_Giros anular=new Anulacion_Giros();
        anular.anulacion();

        Consulta_Giros consulta=new Consulta_Giros();
        consulta.consulta();

    }
    @After
    public void salir()
    {
        Reporte.finReporte();
    }
}
