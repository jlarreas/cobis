package RunScripts.TSERVI;

import Globales.Reporte;
import Globales.Util;
import TestPages.TSERVI.Login;
import TestPages.TSERVI.Pago_Transferencias;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.net.MalformedURLException;

public class RunPago_Transferencia {
    @Before
    public void inicio_Cobis() throws MalformedURLException {
        Util.Inicio("Pago Transferencia ");
        Reporte.setEntorno("Ambiente: Desarrollo1" + "</b><br>" + "Modulo: Cobis-Tservi" );
    }

    @Test
    public void Pago_Transferencia() throws InterruptedException {
        Reporte.setNombreReporte("Pago Transferencia ");
        Login login=new Login();
        login.ingresar();
        Pago_Transferencias pagar=new Pago_Transferencias();
        pagar.pago();
    }

    @After
    public void salir()
    {
        Reporte.finReporte();
    }


}
