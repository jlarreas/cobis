package RunScripts.TSERVI;

import Globales.Reporte;
import Globales.Util;
import TestPages.TSERVI.Login;
import TestPages.TSERVI.Solicitud_Certificado;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.net.MalformedURLException;

public class RunSolicitudCertificado {

    @Before
    public void inicio_Cobis() throws MalformedURLException {
        Util.Inicio("Solicitud de Certificado de Giros");
        Reporte.setEntorno("Ambiente: Desarrollo1" + "</b><br>" + "Modulo: Cobis-Tservi " );
    }

    @Test
    public void Solicitud_Certificado() throws InterruptedException {
        Reporte.setNombreReporte("Solicitud de Certificado de Giros");
        Login login=new Login();
        login.ingresar();
        Solicitud_Certificado solicitud=new Solicitud_Certificado();
        solicitud.generar();
    }

    @After
    public void salir()
    {
        Reporte.finReporte();
    }
}
