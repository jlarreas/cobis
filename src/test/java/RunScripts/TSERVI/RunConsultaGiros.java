package RunScripts.TSERVI;

import Globales.Reporte;
import Globales.Util;
import TestPages.TSERVI.Consulta_Giros;
import TestPages.TSERVI.Login;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.net.MalformedURLException;

public class RunConsultaGiros {

    @Before
    public void inicio_Cobis() throws MalformedURLException {
        Util.Inicio("Consulta Giros");
        Reporte.setEntorno("Ambiente: Desarrollo1" + "</b><br>" + "Modulo: Cobis-Tservi " );
    }

    @Test
    public void Consular_Giros() throws InterruptedException {
        Reporte.setNombreReporte("Consulta Exitosa");
        Login login=new Login();
        login.ingresar();
        Consulta_Giros consultar=new Consulta_Giros();
        consultar.consulta();
    }

    @After
    public void salir()
    {
        Reporte.finReporte();
    }
}

