package RunScripts.TSERVI;
import Globales.Reporte;
import Globales.Util;
import TestPages.TSERVI.ConsultaTransferenciaExterior;
import TestPages.TSERVI.Login;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.net.MalformedURLException;

public class RunConsultarTrasnferenciaEnviadaExterior {
    @Before
    public void inicio_Cobis() throws MalformedURLException {
        Util.Inicio("Consulta Transferencia Exterior");
        Reporte.setEntorno("Ambiente: Desarrollo1" + "</b><br>" + "Modulo: Cobis-Tservi" );
    }

    @Test
    public void Consultar_TransferenciaExterior() throws InterruptedException {
        Reporte.setNombreReporte("Consulta Transferencia Exterior");
        Login login=new Login();
        login.ingresar();
        ConsultaTransferenciaExterior consultar=new ConsultaTransferenciaExterior();
        consultar.consultar_transf_exterior();
    }

    @After
    public void salir()
    {
        Reporte.finReporte();
    }
}

