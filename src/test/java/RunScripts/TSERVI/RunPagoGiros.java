package RunScripts.TSERVI;

import Globales.Reporte;
import Globales.Util;
import TestPages.TSERVI.Login;
import TestPages.TSERVI.Pago_Giros;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.net.MalformedURLException;

public class RunPagoGiros {

    @Before
    public void InicioTservi() throws MalformedURLException {
        Util.Inicio("Pago Giros");
        Reporte.setEntorno("Ambiente: Desarrollo1" + "</b><br>" + "Modulo: Cobis-Tservi " );
    }
    @Test
    public  void Pago_Giros() throws InterruptedException {
        Reporte.setNombreReporte("Anulación Exitosa");
        Login login=new Login();
        login.ingresar();
        Pago_Giros pago=new Pago_Giros();
        pago.pago_giros();
    }

    @After
    public void salir()
    {
        Reporte.finReporte();
    }
}

