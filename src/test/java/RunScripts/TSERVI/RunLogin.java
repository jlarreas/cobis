package RunScripts.TSERVI;
import Globales.Reporte;
import Globales.Util;
import TestPages.TSERVI.Login;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.net.MalformedURLException;

public class RunLogin {

    @Before
    public void iniciar_Chrome() throws MalformedURLException {
        Util.Inicio("Login");
        Reporte.setEntorno("Ambiente: Desarrollo1" + "</b><br>" + "Modulo: CobisTservi " );
    }

    @Test
    public void  LoginTest() throws InterruptedException {
        Reporte.setNombreReporte("Login Exitoso");
        Login login = new Login();
        login.ingresar();

    }

    @After
    public void salir()
    {
        Reporte.finReporte();
    }
}

